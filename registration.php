<?php
require "./config.php";

$postData = file_get_contents("php://input");
$data = json_decode($postData, true);

$login = strip_tags(trim( $data["login"] ));
$pwd = strip_tags(trim( $data["pwd"] ));
$rw_pwd = strip_tags(trim( $data["rw_pwd"] ));

if( !empty($login) && !empty($pwd) && !empty($rw_pwd) ){
  $sql_check = "SELECT EXISTS(SELECT login FROM users WHERE login = :login)";
  $stmt_check = $pdo->prepare($sql_check);
  $stmt_check->execute([":login" => $login]);

  if( $stmt_check->fetchColumn() ){
    $res = array(
      "response" => "Пользователь с таким логином уже существует",
      "code" => 400,
      "success" => true,
    );

    echo json_encode($res);
    die;
  }

  if($pwd == $rw_pwd){
    $pwd = password_hash($pwd, PASSWORD_DEFAULT);
    $sql = "INSERT INTO users(login, pwd, admin) VALUES (:login, :pwd, false)";
    $params = [":login" => $login, ":pwd" => $pwd];
    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    $res = array(
      "response" => "Вы успешно зарегистрировались!",
      "code" => 200,
      "success" => true,
    );

    echo json_encode($res);
    exit;
  } else {
    $res = array(
      "response" => "Пароли не совпадают!",
      "code" => 400,
      "success" => true,
    );
    echo json_encode($res);
  }
} else {
  $res = array(
    "response" => "Пожалуйста заполните все поля!",
    "code" => 400,
    "success" => true,
  );
  echo json_encode($res);
}
?>