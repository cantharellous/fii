<?php
if ($page == "lk" && $_SESSION["access"] == 0) {
  require "./pages/503.html";
  require "./block/footer.html";
}
?>
<div class="content">
  <div class="container lk">
    <h1 class="m10">Слайдер</h1>
    <div class="slides">
      <?php
        require "./config.php";

        $sql = 'SELECT * FROM slider LIMIT 6';
        $stmt = $pdo->query($sql);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $key => $value) {
          echo '
            <div class="slides__slide">
              <div class="slides__media">
                <img src="images/slider/' . $value["image"] . '" class="slides__image" alt="' . $value["image"] . '">
              </div>
              <div class="slides__content">
                <h5 class="slides__content__title">' . $value["title"] . '</h5>
                <p class="slides__content__info">' . $value["text"] . '</p>
              </div>
              <input type="button" value="Удалить" onclick="deleteSlide('.$value["id"].')">
            </div>
          ';
        }
      ?>
    </div>
    
    <div class="add-slide m10">
      <div>
        <form class="auth__block">
          <h1>Добавить слайд</h1>
          <input type="text" name="title" placeholder="Заголовок" required>
          <textarea name="text" cols="30" rows="5" placeholder="Текст"></textarea>
          <label for="file" class="custom-upload">
            <span class="custom-upload__text">Выбрать файл</span>
            <span class="custom-upload__result"></span>
            <input type="file" id="file" name="file">
          </label>
          <input type="button" value="Добавить" onclick="addSlide()">
        </form>
      </div>
    </div>


    <h1 class="mt100">Управление пользователями</h1>
    <table class="mt100 table">
      <thead class="thead-dark">
        <tr>
          <td>#</td>
          <td>Логин</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php
          $sql = 'SELECT id, login FROM users';
          $stmt = $pdo->query($sql);
          $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

          foreach ($rows as $key => $value) {
            echo '
              <tr>
                <td>'.$value["id"].'</td>
                <td>'.$value["login"].'</td>
                <td>
                  <a style="cursor:pointer;" onclick="deleteUser('.$value["id"].')">Удалить</a>
                </td>
              </tr>
            ';
          }
        ?>
      </tbody>
    </table>

    <input type="button" value="Добавить" class="m10" onclick="showAddUser()">
    <div class="add-user hide">
      <form class="auth__block">
        <h1>Создать пользователя</h1>
        <input type="text" name="login" placeholder="Логин" required>
        <input type="password" name="pwd" placeholder="Пароль" required>
        <input type="password" name="rw_pwd" placeholder="Подтвердите пароль" required>
        <input type="button" value="Создать" onclick="addUser()">
      </form>
    </div>
  </div>

</div>

<script src="./admin/js/main.js"></script>