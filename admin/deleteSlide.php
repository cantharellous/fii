<?php
require '../config.php';

$postData = file_get_contents("php://input");
$data = json_decode($postData, true);
$id = strip_tags(trim($data['id']));

$sql = 'DELETE FROM slider WHERE id = :id' ;
$params = [':id' => $id];

$stmt = $pdo->prepare($sql);
$stmt->execute($params);

$res = array(
  "response" => "Слайд удален",
  "code" => 200,
  "success" => true,
);

echo json_encode($res);

?>