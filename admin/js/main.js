async function postData(url = '', data = {}, headers = { 'Content-Type': 'application/json' }) {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors', 
    cache: 'no-cache', 
    credentials: 'same-origin', 
    headers: headers,
    redirect: 'follow', 
    referrerPolicy: 'no-referrer', 
    body: typeof data === "object" ? JSON.stringify(data) : data 
  });
  return response.json(); 
}
async function fileSave() {
  const file = document.getElementById("file").files[0];
  const title = document.querySelectorAll('[name="title"]')[0].value;
  const text = document.querySelectorAll('[name="text"]')[0].value;
  if (!file) {
    alert("Выберите файл!");
    return;
  }
  let fd = new FormData();
  fd.append("file", file);
  fd.append("title", title);
  fd.append("text", text);

  const res = await fetch('./admin/uploadFile.php', {
    method: 'POST',
    body: fd
  });
  return res.json();
}
function deleteSlide(id) {
  postData('./admin/deleteSlide.php', { id: id })
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.reload();
      }
    })
    .catch(err => alert(err.response));
}

function addSlide() {
  fileSave()
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.reload();
      }
    });
}

function deleteUser(id) {
  postData('./admin/deleteUser.php', { id: id }, { 'Content-Type': 'multipart/form-data'})
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.reload();
      }
    })
    .catch(err => alert(err.response));
}

function showAddUser() {
  const addBlock = document.querySelector(".add-user");

  addBlock.classList.remove("hide");
  addBlock.classList.add("show");
}

function addUser() {
  const login = document.querySelectorAll('[name="login"]')[0].value;
  const pwd = document.querySelectorAll('[name="pwd"]')[0].value;
  const rw_pwd = document.querySelectorAll('[name="rw_pwd"]')[0].value;

  if (!login.match(/^[a-zA-Z0-9]+$/)) {
    alert("Логин должен содержать латинские буквы и цифры!");
    return;
  }
  if (!pwd.match(/^[a-zA-Z0-9]+$/) || !rw_pwd.match(/^[a-zA-Z0-9]+$/)) {
    alert("Пароль должен содержать латинские буквы и цифры!");
    return;
  }
  if (pwd.length <= 5 || rw_pwd.length <= 5) {
    alert("Пароль должен быть длинной не менее 6 символов!")
    return;
  }

  postData('./registration.php', { login: login, pwd: pwd, rw_pwd: rw_pwd })
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.reload();
      }
    })
    .catch(err => alert(err.response));
}
function logout() {
  postData('./logout.php')
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.replace("/");
      }
    })
    .catch(err => alert(err.response));
}
