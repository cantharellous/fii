<?php

require "../config.php";

if (isset($_FILES['file']['name'])) {
  $title = strip_tags(trim($_POST["title"]));
  $text = strip_tags(trim($_POST["text"]));
  $filename = $_FILES['file']['name'];
  $valid = array("jpg", "jpeg", "png");
  $extension = pathinfo($filename, PATHINFO_EXTENSION);

  if (in_array($extension, $valid)) {
    $new_name = time() . "." . $extension;
    $targetPath = "../images/slider/" . $new_name;

    if (move_uploaded_file($_FILES['file']['tmp_name'], $targetPath));

    $sql = "INSERT INTO slider(image, title, text) VALUES (:image, :title, :text)";
    $params = [":image" => $new_name, ":title" => $title, ":text" => $text];
    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);
  }

  $res = array(
    "response" => "Слайд успешно добавлен",
    "code" => 200,
    "success" => false
  );
  echo json_encode($res); 

  move_uploaded_file($_FILES["file"]["tmp_name"], $targetPath);
} else {
  $res = array(
    "response" => "Файл не выбран",
    "code" => 400,
    "success" => false,
  );

  echo json_encode($res);
}

?>