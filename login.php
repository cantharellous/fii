<?php
session_start(); 
require_once "./config.php";

$postData = file_get_contents("php://input");
$data = json_decode($postData, true);
$login = strip_tags(trim( $data["login"] ));
$pwd = strip_tags(trim( $data["pwd"] ));

if( !empty($login) && !empty($pwd) ){
  $sql = "SELECT id, login, pwd, admin FROM users WHERE login = :login";
  $params = [":login" => $login];
  
  $stmt = $pdo->prepare($sql);
  $stmt->execute($params);

  $user = $stmt->fetch(PDO::FETCH_OBJ);
  
  if($user){
    if(password_verify($pwd, $user->pwd)){
      $_SESSION["user_id"] = $user->id;
      $_SESSION["access"] = $user->admin;
      $_SESSION["user_login"] = $user->login;

      $res = array(
        "response" => "Авторизация произведена успешно",
        "code" => 200,
        "success" => true,
      );

      echo json_encode($res);
    }else{
      $res = array(
        "response" => "Неверный логин или пароль!",
        "code" => 400,
        "success" => true,
      );

      echo json_encode($res);
    }
  } else {
    $res = array(
      "response" => "Неверный логин или пароль!",
      "code" => 400,
      "success" => true,
    );

    echo json_encode($res);
  }
} else {
  $res = array(
    "response" => "Пожалуйста заполните все поля!",
    "code" => 400,
    "success" => true,
  );

  echo json_encode($res);
}
?>