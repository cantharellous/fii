<?php
  session_start();
  $_SESSION = [];

  $res = array(
    "response" => "Вы вышли из аккаунта",
    "success" => true,
    "code" => 200
  );

  echo json_encode($res);