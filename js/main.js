const slide = document.querySelectorAll(".slide");
const slider = document.querySelector(".slider");
let count = 0;
let trfx = 0;
let w;
let timeout = 5000;
let img, div, divCh, t_img;

function ready() {
  w = slider.offsetWidth;
  trfx = w;

  // slider.style.width = `${slide.length * w}px`;

  slide.forEach(el => {
    el.classList.add("hide");
    t_img = el.querySelectorAll(".slide__ch");
    t_img.forEach((el, i) => {
      el.style.top = `${i * 3}0%`;
    });
    div = document.createElement("div");
    div.classList = "slide__media";
    el.append(div);

    for (let i = 0; i < 3; i++) {
      divCh = document.createElement("div");
      divCh.classList = "slide__ch slide__ch-" + i;
      img = el.querySelector(".slide__image");
      img.style.cssText = `width: ${w}px`;
      divCh.append(img.cloneNode(true));
      div.append(divCh);
    }
    img.remove();
  });
  slide[count].classList.add("show");
  slide[count].classList.remove("hide");
}
document.addEventListener("DOMContentLoaded", ready);

function steps(s) {
  t_img = slide[count].querySelectorAll(".slide__ch");
  t_img.forEach((el, i) => {
    el.style.top = `-${i * 3}0%`;
  });
  slide[count].classList.add("hide");
  slide[count].classList.remove("show");
  setTimeout(() => {
    t_img.forEach((el, i) => {
      el.style.top = `0`;
    });
  }, 1000)
  
  if (s === "next") count++;
  if (s === "prev") count--;

  if (count <= -1) {
    count = slide.length - 1;
  }
  if (count >= slide.length) {
    count = 0;
  }

  console.log(count);
  setTimeout(() => {
    t_img = slide[count].querySelectorAll(".slide__ch");
    slide[count].classList.remove("hide");
    slide[count].classList.add("show");
    t_img.forEach((el, i) => {
      el.style.top = `0`;
    });
  }, 1000);
}

async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors', 
    cache: 'no-cache', 
    credentials: 'same-origin', 
    headers: {
      'Content-Type': 'application/json'
      
    },
    redirect: 'follow', 
    referrerPolicy: 'no-referrer', 
    body: JSON.stringify(data) 
  });
  return response.json(); 
}


function registration() {
  const login = document.querySelectorAll('[name="login"]')[0].value;
  const pwd = document.querySelectorAll('[name="pwd"]')[0].value;
  const rw_pwd = document.querySelectorAll('[name="rw_pwd"]')[0].value;

  if (!login.match(/^[a-zA-Z0-9]+$/)) {
    alert("Логин должен содержать латинские буквы и цифры!");
    return;
  }
  if (!pwd.match(/^[a-zA-Z0-9]+$/) || !rw_pwd.match(/^[a-zA-Z0-9]+$/)) {
    alert("Пароль должен содержать латинские буквы и цифры!");
    return;
  }
  if (pwd.length <= 5 || rw_pwd.length <= 5) {
    alert("Пароль должен быть длинной не менее 6 символов!")
    return;
  }

  postData('./registration.php', { login: login, pwd: pwd, rw_pwd: rw_pwd })
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.replace("/");
      }
    })
    .catch(err => alert(err.response));
}

function auth() {
  const login = document.querySelectorAll('[name="login"]')[0].value;
  const pwd = document.querySelectorAll('[name="pwd"]')[0].value;

  if (!login.match(/^[a-zA-Z0-9]+$/)) {
    alert("Логин должен содержать латинские буквы и цифры!");
    return;
  }
  if (!pwd.match(/^[a-zA-Z0-9]+$/)) {
    alert("Пароль должен содержать латинские буквы и цифры!");
    return;
  }
  if (pwd.length <= 5) {
    alert("Пароль должен быть длинной не менее 6 символов!")
    return;
  }

  postData('./login.php', { login: login, pwd: pwd })
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.replace("/");
      }
    })
    .catch(err => alert(err.response));
}

function logout() {
  postData('./logout.php')
    .then(data => {
      alert(data.response);
      if(data.code === 200) {
        window.location.replace("/");
      }
    })
    .catch(err => alert(err.response));
}
