<?php
  $HOST = "localhost"; 
  $USER = "root"; 
  $PASS = "root"; 
  $DRIVER  = "mysql"; 
  $DB_NAME = "fii"; 
  $CHARSET = "utf8";

  $OPTIONS = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

  try {
    $pdo = new PDO("$DRIVER:host=$HOST;dbname=$DB_NAME;charset=$CHARSET",$USER, $PASS, $OPTIONS);  
  } catch (PDOException $e) {
    require "./block/head.php";
    require "./block/header.php";
    require "./pages/db_err.html";
    require "./block/footer.php";
    die;
  }
?>