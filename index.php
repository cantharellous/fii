<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
// require "./config.php";
require "./block/head.php";
require "./block/header.php";

if($_SERVER["REQUEST_URI"] == "/"){
  $page = "index";
} else {
  $page = substr($_SERVER["REQUEST_URI"], 1);
}

if ($page == "portfolio" && !isset($_SESSION["user_id"])) {
  $page = "503";
}

if ($page == "lk" && $_SESSION["access"] == 1) {
  require "admin/lk.php";
  die;
}

if( file_exists("./pages/$page.php") ) {
  require "./pages/$page.php";
} else {
  require "./pages/404.php";
}


require "./block/footer.php"; ?>