<div class="content">
  <div class="container auth">
    <form class="auth__block">
      <?php
        if (isset($_SESSION["err_auth"])) {
          echo "Попыток авторизации: " .$_SESSION["err_auth"] . " / 5";
        }
      ?>
      <h1>Авторизация</h1>
      <input type="text" name="login" placeholder="Логин" required>
      <input type="password" name="pwd" placeholder="Пароль" required>
      <input type="button" value="Авторизоваться" onclick="auth()">
      <a href="/registration">У меня нет аккаунта</a>
    </form>
  </div>
</div>