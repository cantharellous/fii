<div class="content">
  <div class="container auth">
    <form class="auth__block">
      <h1>Регистрация</h1>
      <input type="text" name="login" placeholder="Логин" required>
      <input type="password" name="pwd" placeholder="Пароль" required>
      <input type="password" name="rw_pwd" placeholder="Подтвердите пароль" required>
      <input type="button" value="Создать" onclick="registration()">
      <a href="/login">У меня есть аккаунт</a>
    </form>
  </div>
</div>