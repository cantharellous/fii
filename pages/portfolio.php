<?php
  require "./config.php";

  $sql = 'SELECT * FROM portfolio';
  $stmt = $pdo->query($sql);
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="content">
  <?php foreach ($rows as $key => $value) {?>
  <div class="container mt100">
    <h1 class="m10"><? echo $value["title"] ?></h1>
    <div class="gallery">
      <div class="gallery__block">
        <div class="gallery__row">
          <div class="gallery__media">
            <div class="gallery__img-block">
              <img src="images/gallery/<? echo $value['img1'] ?>" alt="">
            </div>
          </div>
          <div class="gallery__media">
            <div class="gallery__img-block">
              <img src="images/gallery/<? echo $value['img2'] ?>" alt="">
            </div>
          </div>
        </div>
        <div class="gallery__media gallery__media-big">
          <div class="gallery__img-block">
            <img src="images/gallery/<? echo $value['img3'] ?>" alt="">
          </div>
        </div>
      </div>
      <div class="gallery__block" style="width: 50.7%">
        <div class="gallery__media gallery__media-big" height="618px">
          <div class="gallery__img-block">
            <img src="images/gallery/<? echo $value['img4'] ?>" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
  <? } ?>
</div>
