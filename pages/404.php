<div class="content">
  <div class="container error">
    <div class="error__row">
      <div class="error__content">
        <h1>Упс, что-то пошло не так</h1>
        <p>Код ошибки: <b>404</b></p>
        <p>Возможно Вам необходимо <a href="/login">Авторизироваться</a></p>
      </div>
      <img src="../images/err/503.PNG" alt="">
    </div>
  </div>
</div>