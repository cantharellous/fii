<?php
  require "./config.php";

  $sql = 'SELECT * FROM reviews';
  $stmt = $pdo->query($sql);
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="content">
  <div class="container reviews">
    <h1 class="reviews__title">Отзывы</h1>

    <div class="reviews__row">
      <?php
        foreach ($rows as $key => $value) {
          echo '
            <div class="reviews__block">
              <div class="reviews__usr">
                <div class="reviews__usr-media">
                  <img src="images/footer/'.$value["avatar"].'" alt="'.$value["name"].'" class="reviews__avatar">
                </div>
                <p class="reviews__usr-name">'.$value["name"].'</p>
              </div>
              <div class="reviews__text">
              
                <p><img style="margin: 0 15px 15px 0;" src="data:image/svg+xml;base64,Cjxzdmcgd2lkdGg9IjIxIiBoZWlnaHQ9IjEzIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgICA8cGF0aCB0cmFuc2Zvcm09InNjYWxlKDEpIiBmaWxsPSIjMDAwMDAwIiBkPSJNNS4zLDQuN2MwLjQtMS43LDEuNi0yLjQsMy0yLjhjMC4xLDAsMC4xLTAuMiwwLjEtMC4yTDguMiwwLjNjMCwwLDAtMC4xLTAuMi0wLjFDMy4yLDAuNywwLDQuNCwwLjYsOC44CgkJYzAuNiwzLjEsMyw0LjMsNS4yLDMuOWMyLjItMC40LDMuNy0yLjQsMy40LTQuNkM4LjgsNi4yLDcuMiw0LjgsNS4zLDQuN3oiPjwvcGF0aD4KICAgIDxwYXRoIHRyYW5zZm9ybT0ic2NhbGUoMSkiIGZpbGw9IiMwMDAwMDAiIGQ9Ik0yMC41LDguMWMtMC4zLTEuOS0xLjktMy4zLTMuOC0zLjRjMC41LTEuNywxLjYtMi40LDMtMi44YzAuMSwwLDAuMS0wLjIsMC4xLTAuMmwtMC4yLTEuNGMwLDAsMC0wLjEtMC4yLTAuMQoJCWMtNC44LDAuNS04LDQuMi03LjQsOC42YzAuNiwzLDIuOSw0LjIsNS4xLDMuOUMxOS4zLDEyLjQsMjAuOCwxMC4zLDIwLjUsOC4xeiI+PC9wYXRoPgo8L3N2Zz4K" alt="" />'.$value["text"].'</p>
              </div>
            </div>
          ';
        }
      ?>
    </div>
  </div>
</div>
