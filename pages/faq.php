<div class="content">
  <div class="container">
    <div class="faq">
      <?php
        require "./config.php";

        $sql = 'SELECT * FROM faq';
        $stmt = $pdo->query($sql);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows as $key => $value) {
          echo '
            <div class="faq__question-block">
              <p class="faq__question">
                <img src="images/icons/u145.svg" alt="">
                ' . $value["question"] . '</p>
              <div class="faq__answer-block">
                <p class="faq__answer">' . $value["answer"] . '</p>
              </div>
            </div>
          ';
        }
      ?>
    </div>
  </div>
</div>