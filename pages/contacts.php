<?php
  require "./config.php";

  $sql = 'SELECT * FROM contacts';
  $stmt = $pdo->query($sql);
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="content">
  <div class="container contacts">
    <div class="contacts__content">
      <h1 class="contacts__title">Контакты</h1>
      <p><b>Адрес:</b> <? echo $row["adress"] ?></p>
      <p><b>Телефон:</b> <? echo $row["number"] ?></p>
    </div>
    <div class="socials">
      <h2 class="contacts__title">Социальные сети</h2>
      <div class="socials__icons">
        <a href="<? echo $row["vk_link"] ?>" class="socials__link">
          <img src="images/icons/u54.svg" alt="">
        </a>
        <a href="<? echo $row["tg_link"] ?>" class="socials__link">
          <img src="images/icons/u55.svg" alt="">
        </a>
        <a href="<? echo $row["tw_link"] ?>" class="socials__link">
          <img src="images/icons/u56.svg" alt="">
        </a>
        <a href="<? echo $row["inst_link"] ?>" class="socials__link">
          <img src="images/icons/u57.svg" alt="">
        </a>
      </div>
    </div>
  </div>

  <script type="text/javascript" charset="utf-8" async src="<? echo $row["link_api_map"] ?>"></script>
</div>
