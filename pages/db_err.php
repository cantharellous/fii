<div class="content">
  <div class="container error">
    <div class="error__row">
      <div class="error__content">
        <h1>Сервис недоступен</h1>
        <p>Код ошибки: <b>503</b></p>
        <p>Ошибка подключения к Базе Данных</p>
      </div>
      <img src="../images/err/503.PNG" alt="">
    </div>
  </div>
</div>