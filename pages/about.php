<?php
  require "./config.php";

  $sql = 'SELECT * FROM about';
  $stmt = $pdo->query($sql);
  $row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<div class="content">
  <div class="container our">
    <div class="our__media">
      <img src="images/<?php echo $row["image"] ?>" alt="">
    </div>
    <div class="our__content">
      <h1><?php echo $row["title"] ?></h1>
      <?php echo $row["text"] ?>
    </div>
  </div>
</div>
