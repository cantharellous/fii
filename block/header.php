<?php
  $page = substr($_SERVER["REQUEST_URI"], 1);

  if($_SESSION["access"] == 1) {
    $page_lk = "/lk";
  } else {
    $page_lk = "/auth-service";
  }


  if ($page == "lk" && $_SESSION["access"] == 1) {
?>

<header class="header">
  <div class="container row justify-content-between" style="padding: 10px 0;">
    Админ панель
    <div>
      Админ / <a onclick="logout()">Выйти</a>
    </div>
  </div>
</header>

<?php 
  } else {
?>
<header class="header">
  <nav class="nav">
    <ul>
      <li><a href="/">Главная</a></li>
      <li><a href="/about">О нас</a></li>
      <li><a href="/portfolio">Портфолио</a></li>
    </ul>
    <a href="/" class="logo">
      <img src="images/logo.png" alt="Logo">
    </a>
    <ul>
      <li><a href="/contacts">Контакты</a></li>
      <li><a href="/reviews">Отзывы</a></li>
      <li><a href="/faq">FAQ</a></li>
    </ul>
  </nav>
  <div class="auth-block">
    <?php
      if (!isset($_SESSION["user_id"])) {
        echo '
          <ul>
            <li><a href="'. $page_lk .'">Личный кабинет</a></li>
          </ul>
        ';
      } else {
        echo '
          <ul>
            <li><a href="'. $page_lk .'">Личный кабинет</a></li>
            <li>/</li>
            <li><a onclick="logout()">Выйти</a></li>
          </ul>
        ';
      }
    ?>
  </div>
</header>

<?php } ?>