
  <footer class="footer">
    <div class="footer__row">
      <div class="footer__media">
        <img src="images/footer/1.jpg" alt="">
      </div>
      <div class="footer__media">
        <img src="images/footer/2.jpg" alt="">
      </div>
      <div class="footer__media">
        <img src="images/footer/3.jpg" alt="">
      </div>
      <div class="footer__media">
        <img src="images/footer/4.jpg" alt="">
      </div>
      <div class="footer__media">
        <img src="images/footer/5.jpg" alt="">
      </div>
      <div class="footer__media">
        <img src="images/footer/6.jpg" alt="">
      </div>
      <button class="btn">@fii_phgrph</button>
    </div>
    <div class="container footer__bottom">
      <a href="#" class="logo">
        <img src="images/logo.png" alt="logo">
      </a>
      <div class="copyright">
        © IMAGES BY HERR HOLZNER
      </div>
      <a class="logo" style="opacity: 0;">
        <img src="images/logo.png" alt="logo">
      </a>
    </div>
  </footer>

  <script src="js/main.js"></script>
</body>
</html>