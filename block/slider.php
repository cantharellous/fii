<div class="slider-wrap">
  <div class="slider">
    <?php
      require "./config.php";

      $sql = 'SELECT * FROM slider LIMIT 6';
      $stmt = $pdo->query($sql);
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

      foreach ($rows as $key => $value) {
        echo '
          <div class="slide">
            <img src="images/slider/' . $value["image"] . '" class="slide__image" alt="' . $value["image"] . '">
            <div class="slide__content">
              <h1 class="slide__content__title">' . $value["title"] . '</h1>
              <p class="slide__content__info">' . $value["text"] . '</p>
            </div>
          </div>
        ';
      }
    ?>
  </div>
  <div class="row justify-content-center">
    <button onclick="steps('prev')" class="btn btn-next">
      &#10094;
    </button>
    <button onclick="steps('next')" class="btn btn-next">
      &#10095;
    </button>
  </div>
</div>